<?php

function moh_d3_help_delegate($path, $arg) {
  global $base_url;

  switch ($path) {
    case 'admin/help#moh_d3':
      $output =
          '<h3>' . t('Introduction') . '</h3>' .
          '<p>' .
          t('The CKEditor module allows Drupal to replace textarea fields with CKEditor. CKEditor is an online rich text editor that can be embedded inside web pages. It is a !wysiwyg editor which means that the text edited in it looks as similar as possible to the results end users will see after the document gets published. It brings to the Web popular editing features found in desktop word processors such as Microsoft Word and OpenOffice.org Writer. CKEditor is truly lightweight and does not require any kind of installation on the client computer.', array(
            '!wysiwyg' => '<acronym title="' . t('What You See Is What You Get') . '">' . t('WYSIWYG') . '</acronym>',
              )
          ) .
          '</p>' .
          '<p>' .
          t('Useful links: !ckeditorlink | !devguidelink | !userguidelink.', array(
            '!ckeditorlink' => l(t('CKEditor website'), 'http://ckeditor.com'),
            '!devguidelink' => l(t('CKEditor for Drupal 7 Documentation'), 'http://docs.cksource.com/CKEditor_for_Drupal/Open_Source/Drupal_7'),
            '!userguidelink' => l(t('User\'s Guide'), 'http://docs.cksource.com/CKEditor_3.x/Users_Guide')
              )
          ) .
          '</p>' .
          '<h4>' .
          t('Configuration') .
          '</h4>' .
          '<ol>' .
          '<li>' .
          t('CKEditor profiles can be configured in the !adminpath section. Profiles determine which options are available to users based on the input format system.', array(
            '!adminpath' => '<strong>' . l(t('Administration panel') . ' > ' . t('Configuration') . ' > ' . t('Content Authoring') . ' > ' . t('CKEditor'), 'admin/config/content/ckeditor') . '</strong>'
              )
          ) .
          '</li>' .
          '<li>' .
          t('For the Rich Text Editing to work you also need to configure your !filterlink for the users that may access Rich Text Editing. Either grant those users <strong>Full HTML</strong> access or use the following list of tags in the HTML filter:', array(
            '!filterlink' => l(t('filters'), 'admin/config/content/formats')
              )
          ) .
          '<br /><code>' .
          htmlspecialchars('<a> <p> <span> <div> <h1> <h2> <h3> <h4> <h5> <h6> <img> <map> <area> <hr> <br> <br /> <ul> <ol> <li> <dl> <dt> <dd> <table> <caption> <tbody> <tr> <td> <em> <b> <u> <i> <strong> <del> <ins> <sub> <sup> <quote> <blockquote> <pre> <address> <code> <cite> <embed> <object> <param> <strike>') .
          '</code><br />' .
          t('<strong>Note:</strong> be careful when granting users access to create tags like %iframe.<br />If you are going to use CKEditor with the <strong>Filtered HTML</strong> input format, please read the "Setting up filters" section in the !readme file.', array(
            '%iframe' => '<iframe>',
            '!readme' => '<code>' . l(t('README.txt'), $base_url . '/' . drupal_get_path('module', 'ckeditor') . '/README.txt', array('absolute' => TRUE)) . '</code>'
              )
          ) .
          '</li>' .
          '<li>' .
          t('To have better control over line breaks, you should disable the <strong>%settingname</strong> setting in the chosen Text format (recommended).', array(
            '%settingname' => t('Line break converter'),
              )
          ) .
          '</li>' .
          '<li>' .
          t('All configuration options described in the !apidocs that cannot be easily changed in the administration area can be set in the <strong>Advanced Options</strong> section in the CKEditor profile.', array(
            '!apidocs' => l(t('API documentation'), 'http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html')
              )
          ) .
          '</li>' .
          '</ol>' .
          '<h3>' .
          t('Troubleshooting') .
          '</h3>' .
          '<p>' .
          t('Take a look at !listlink when installing CKEditor.', array(
            '!listlink' => l(t('the list of common problems'), 'http://docs.cksource.com/CKEditor_for_Drupal/Open_Source/Drupal_7/Troubleshooting')
              )
          ) .
          ' ' .
          t('If you are looking for more information, have any trouble with the configuration, or found an issue with the CKEditor module, please visit the !officiallink.', array(
            '!officiallink' => l(t('official project page'), 'http://drupal.org/project/ckeditor')
              )
          ) .
          ' ' .
          t('More information about how to customize CKEditor for your theme can be found !herelink.', array(
            '!herelink' => l(t('here'), 'http://docs.cksource.com/CKEditor_for_Drupal/Open_Source/Drupal_7/Tricks')
              )
          ) .
          '</p>' .
          '<h3>' .
          t('Plugins: Code Snippet and MathJax') .
          '</h3>' .
          '<p>' .
          t('Code Snippet and MathJax are special plugins for CKEditor that are using external JavaScript libraries to style content inside editing area. The result that is returned by CKEditor is just an HTML tag that needs to again processed by a filter (either server side or client side) in order to display it properly to the user.') .
          '</p>' .
          '<h4><a name="mathjax"></a>' .
          t('MathJax (Mathematical Formulas)') .
          '</h4>' .
          '<p>' .
          t('With mathjax plugin, CKEditor produces LaTeX code surrounded by !code. In order to have it properly rendered on your site you might need to add !mathjax on your website, the simplest way to do this is to add this to your theme: !script', array(
            '!code' => '<code>'.htmlspecialchars('<span class="math-tex"></span>').'</code>',
            '!mathjax' => l('MathJax', 'http://www.mathjax.org/'),
            '!script' => '<br /><code>'.htmlspecialchars('<script src="http://cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML" type="text/javascript"></script>').'</code>'
          )) .
          '</p>' .
          '<h4><a name="codesnippet"></a>' .
          t('Code Snippet') .
          '</h4>' .
          '<p>' .
          t('With codesnippet plugin, CKEditor produces code snippets surrounded by !code. <strong>Note:</strong> You might need to add !highlight on your website so that the displayed code was rendered nicely as in CKEditor. The simplest way to do this is to add this to your theme: !script', array(
            '!highlight' => l('highlight.js', 'http://highlightjs.org/'),
            '!code' => '<code>'.htmlspecialchars('<pre><code></code></pre>').'</code>',
            '!script' => '<br /><code>'.htmlspecialchars('<link rel="stylesheet" href="http://cdn.ckeditor.com/' . CKEDITOR_LATEST . '/full-all/plugins/codesnippet/lib/highlight/styles/default.css">').'<br />'.
htmlspecialchars('<script src="http://cdn.ckeditor.com/' . CKEDITOR_LATEST . '/full-all/plugins/codesnippet/lib/highlight/highlight.pack.js" type="text/javascript"></script>').'<br />'.
htmlspecialchars('<script>hljs.initHighlightingOnLoad();</script>').'</code>'
          )) .
          '</p>' .
          '<h3>' .
          t('Uploading images and files') .
          '</h3>' .
          '<p>' .
          t('There are three ways for uploading files:') .
          '</p>' .
          '<ol>' .
          '<li>' .
          t('By using !ckfinder (commercial), an advanced Ajax file manager.', array(
            '!ckfinder' => l(t('CKFinder'), 'http://cksource.com/ckfinder'),
              )
          ) .
          '</li>' .
          '<li>' .
          t('By using a dedicated module like !imcelink.', array(
            '!imcelink' => l(t('IMCE'), 'http://drupal.org/project/imce')
              )
          ) .
          '</li>' .
          '<li>' .
          t('By using the core upload module.') .
          '</li>' .
          '</ol>';

      break;
  }
  return !empty($output) ? $output : '';
}
