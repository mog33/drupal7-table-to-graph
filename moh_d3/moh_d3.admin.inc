<?php
/**
 * @file
 * D3 for MoH admin file.
 */

/**
 * D3 for MoH configuration form.
 */
function moh_d3_config_form($form, &$form_state) {
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('D3 global settings'),
    '#description' => t('Specify global settings for every graph, each settings ca be overriden on the table level with specific <em>data-graph-*</em> attributes.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['global']['moh_d3_colors'] = array(
    '#title' => 'Colors',
    '#description' => t('Should be a comma separated list of colors names based on <a href="https://www.w3.org/TR/SVG/types.html#ColorKeywords">W3C color keyword names</a> or hexa or rgb if prefixed.'),
    '#type' => 'textfield',
    '#size' => 90,
    '#required' => TRUE,
    '#default_value' => variable_get('moh_d3_colors', 'blue, red, orange, green'),
  );
  /*
  $form['global']['moh_d3_width'] = array(
    '#title' => 'Width',
    '#description' => t('If empty will fit in content width.'),
    '#type' => 'textfield',
    '#field_suffix' => 'px',
    '#size' => 5,
    '#default_value' => variable_get('moh_d3_width', ''),
  );
  $form['global']['moh_d3_height'] = array(
    '#title' => 'Height',
    '#description' => t('If empty will fit in data height (y axis).'),
    '#type' => 'textfield',
    '#field_suffix' => 'px',
    '#size' => 5,
    '#default_value' => variable_get('moh_d3_height', ''),
  );
  */
  $form['global']['moh_d3_grid_x'] = array(
    '#title' => 'Grid x',
    '#description' => t('Show grid for x axis.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('moh_d3_grid_x', FALSE),
  );
  $form['global']['moh_d3_grid_y'] = array(
    '#title' => 'Grid y',
    '#description' => t('Show grid for y axis.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('moh_d3_grid_y', FALSE),
  );
  $form['global']['moh_d3_legend_position'] = array(
    '#title' => 'Legend position',
    '#type' => 'select',
    '#options' => array(
      'bottom' => t('Bottom'),
      'right' => t('Outside right'),
      'top-right' => t('Inside top right'),
      'bottom-right' => t('Inside bottom right'),
      'top-left' => t('Inside top left'),
      'bottom-left' => t('Inside bottom left'),
    ),
    '#default_value' => variable_get('moh_d3_legend_position', 'bottom'),
  );
  $form['global']['moh_d3_data_label'] = array(
    '#title' => 'Data label',
    '#description' => t('Choose if data value is shown directly on the graph.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('moh_d3_data_label', FALSE),
  );
  $form['global']['moh_d3_tooltip'] = array(
    '#title' => 'Tooltip',
    '#description' => t('Choose if tooltip is set.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('moh_d3_tooltip', FALSE),
  );
  $form['global']['moh_d3_tooltip_grouped'] = array(
    '#title' => 'Grouped tooltips',
    '#description' => t('Choose if tooltip are grouped (all results for the axis).'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('moh_d3_tooltip_grouped', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="moh_d3_tooltip"]' => array('checked' => FALSE),
      ),
    ),
  );
  return system_settings_form($form);
}
