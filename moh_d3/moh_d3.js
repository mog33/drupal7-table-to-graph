/**
 * @file
 * Extend d3 library to support table to chart.
 */

(function($, Drupal) {

  /**
   * Drupal behaviors for D3 Graph from table module.
   */
  Drupal.behaviors.mohD3 = {
    attach: function(context, settings) {

      // Get existing table data.
      $('.table-d3chart-markup svg').each(function(i, e) {

          // Get the table with options.
          var table = $(this).parent('div').siblings('table.d3graph');

          // Add title and desc for accessibility.
          var title = table.attr('data-graph-title');
          var desc = table.attr('data-graph-desc');
          var aria = [];
          if (desc !== undefined && desc.length > 0) {
            $(this).prepend('<desc>' + desc + '</desc>');
            aria.push('desc');
          }
          if (title !== undefined && title.length > 0) {
            $(this).prepend('<title>' + title + '</title>');
            aria.push('title');
          }
          if (aria.length !== 0) {
            $(this).attr('aria-labelledby', aria.join(' '));
          }

      });
    }
  };

})(jQuery, Drupal);
