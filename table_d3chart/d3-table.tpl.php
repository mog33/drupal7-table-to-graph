<?php
/**
 * @file
 * Default theme file for d3 table markup.
 */
 ?>
<?php if ($identifier): ?>
  <?php if ($chart && $identifier): ?>
  <div id="<?php print $identifier; ?>" class="<?php print implode(' ', $classes_array); ?>" data-graph-source="<?php print $source_id; ?>">
    <span class="graph-data hidden element-invisible"><?php print drupal_json_encode($chart); ?></span>
  </div>
  <div class="graph-button-wrapper"><button class="btn btn-info center-block" id="<?php print $identifier; ?>-graph-button"><?php print t('Click to view table data'); ?></button></div>
  <?php endif; ?>
  <span class="<?php print $identifier; ?>-graph-toggle"></span>
<?php else: ?>
  <!-- No identifier for this graph markup -->
<?php endif; ?>
