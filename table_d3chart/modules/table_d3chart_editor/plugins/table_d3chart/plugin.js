/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 *
 * Portions of code:
 * Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */
//console.log('d3');
(function($) {

  CKEDITOR.plugins.add('table_d3chart', {
    requires: 'dialog',
    init: function(editor) {



      CKEDITOR.on('dialogDefinition', function(ev) {

        if ((ev.editor != editor) || (ev.data.name != 'table')) return;

        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        var infoTab = dialogDefinition.getContents( 'advanced' );
        // Add a new text field to the "tab1" tab page.
        infoTab.add( {
          type: 'select',
          label: 'Graph type',
          id: 'grapthType',
          items: [['linegraph'], ['columnchart'], ['barchart'], ['piechart']]
          'default': 'linegraph',
          //validate: function() {
            //if ( ( /\d/ ).test( this.getValue() ) )
              //return 'My Custom Field must not contain digits';
          //}
        });

        console.log(dialogName);
        console.log(dialogDefinition);




      });
    }
  });
})(jQuery);
