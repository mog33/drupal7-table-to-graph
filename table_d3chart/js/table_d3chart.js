/**
 * @file
 * Extend d3 library to support table to chart.
 */

(function($, Drupal) {

  /**
   * Drupal behaviors for D3 Graph from table module.
   */
  Drupal.behaviors.tableD3Chart = {
    attach: function(context, settings) {

      // Get existing table data.
      $('.table-d3chart-markup').each(function(i, e) {

        // Data settings array is stored in a div above the table.
        var data = $.parseJSON($(this).children('.graph-data').text());
        if (data.length != 0) {

          // Rely on D3 js to build the graph.
          var id = $(this).attr('id');
          Drupal.d3.draw(id, data);

          // Set up link if graph.
          if ($(this).has('svg').length) {

            // Handle link to hide/show table.
            var link = $('#' + id + '-graph-button');
            var target = $('.' + id + '-graph-toggle').next('table');
            target.addClass('element-invisible');
            // Button function click handler.
            link.click(function(){
              target.toggleClass('element-invisible');
              $(this).toggleClass('down');
              if (target.hasClass('element-invisible')) {
                $(this).text(Drupal.t('Click to view table data'));
              }
              else {
                $(this).text(Drupal.t('Click to hide table data'));
              }
            });
          }
          else {
            // Hide the button, and add an info because d3 is silent.
            $('#' + id + '-graph-button').hide();
            console.info('Graph svg has not been build from table #' + id);
          }
        }

      });
    }
  };

})(jQuery, Drupal);
