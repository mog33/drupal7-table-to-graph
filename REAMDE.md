Modules to integrate D3.js / C3.js to Drupal 7.
Mangage to handle a table data to a graph.

----

Want some help implementing this on your project? I provide Drupal 7/8 expertise
as a freelance, just [contact me](https://developpeur-drupal.com/en).