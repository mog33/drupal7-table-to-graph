/**
 * @file
 * Javascript for Linear.
 */

(function($, Drupal, undefined) {

  /**
   * Adds library to the global d3 object.
   *
   * @param select
   * @param settings
   *   Array of values passed to d3_draw.
   *   id: required. This will be needed to attach your
   *       visualization to the DOM.
   */
  Drupal.d3.c3 = function (select, datas) {

    // Grab our helpers function.
    var _h = Drupal.d3.helpers;
    // Get global settings merge with this current table overrides.
    var _s =  _h.getSettings(datas.id);

    // Build our options to match C3 options.
    // http://c3js.org/reference.html
    var options = {
      bindto: '#' + select,
      size: {},
      data: {
        labels: _s.graphDataLabel,
        rows: datas.rows_c3,
        regions: {}
      },
      interaction: {
        enabled: _s.graphInteraction
      },
      padding: {},
      color: {},
      legend: {
        show: _s.graphLegend,
        position: _s.graphLegendPosition,
        inset: {}
      },
      tooltip: {
        show: _s.graphTooltip,
        grouped: _s.graphGroupedTooltip
      },
      grid: {
        x: { show: _s.graphGridX },
        y: { show: _s.graphGridY }
      },
      axis: {
        rotated: _s.graphRotated,
        x: {
          show: _s.graphXaxisShow,
          padding: {
            left: 0,
            right: 0,
          },
          label: {
            text: _s.graphXaxisLabel,
            position: 'outer-center'
          },
          tick: {
            //centered: _s.graphXaxisCentered,
            outer: false,
            //fit: true
          }
        },
        y: {
          show: _s.graphYaxisShow,
          padding: {
            top: 10,
            bottom: 10
          },
          inverted: _s.graphYaxisInverted,
          label: {
            text: _s.graphYaxisLabel,
            position: 'outer-middle'
          },
          tick: {
            outer: false
          }
        }
      },
      bar: {
        width: {
          ratio: _s.graphBarRatio
        }
      },
      point: {
        show: _s.graphDot,
        r: _s.graphDotR
      }
    };

    // Colors handling conditionnal option.
    if (_s.graphColors != undefined) {
      if (_s.graphColors instanceof Array) {
        options.color.pattern = _s.graphColors;
      }
      else {
        options.color.pattern = _h.comma2array(_s.graphColors);
      }
    }
    else {
      // Fallback on default colors from settings.
      options.color.pattern = _h.comma2array(_s.graphDefaultColors);
    }
    // Categories check.
    if (datas.categories == '') {
      options.axis.x.type = 'indexed';
    }
    else {
      options.axis.x.type = 'category';
      options.axis.x.categories = datas.categories;
    }

    /** Extra options depending table data-graph- **/

    // Add type override.
    if (_s.C3Types.indexOf(_s.graphRender) != -1) {
      options.data.type = _s.graphRender;
    }
    // Set graph property override.
    if (_s.graphWidth != undefined) {
      options.size.width = _s.graphWidth;
    }
    else if (_s.graphDefaultWidth != '') {
      options.size.width = _s.graphDefaultWidth;
    }
    if (_s.graphHeight != undefined) {
      options.size.height = _s.graphHeight;
    }
    else if (_s.graphDefaultHeight != '') {
      options.size.height = _s.graphDefaultHeight;
    }
    // Padding, optionnal as default is auto.
    if (_s.graphPadding != undefined && _s.graphPadding != '') {
      var padding = _h.comma2array(_s.graphPadding);
      options.padding = {
        top: parseInt(padding[0]),
        right: parseInt(padding[1]),
        bottom: parseInt(padding[2]),
        left: parseInt(padding[3])
      };
    }
    // Create an object of graph type if needed.
    if (_s.graphTypes != '') {
      var types = _h.comma2array(_s.graphTypes);
      var obj = {};
      for(var i = 0, l = datas.legend.length; i < l; i++){
        if (types[i] != undefined) {
          obj[datas.legend[i]] = types[i];
        }
      }
      options.data.types = obj;
    }
    // X ticks settings.
    if (_s.graphXaxisRotation != '' || _s.graphXaxisRotation != '0') {
      options.axis.x.tick.rotate = _s.graphXaxisRotation;
    }
    if (_s.graphXaxisCentered != '' || _s.graphXaxisCentered) {
      options.axis.x.tick.centered = _s.graphXaxisCentered;
    }
    // Sepcific legend inset break settings.
    if (_s.graphLegend) {
      if(_s.graphLegendPosition == 'bottom' || _s.graphLegendPosition == 'right') {
        options.legend.position = _s.graphLegendPosition;
      }
      if (_s.graphLegendPosition == 'top-right'
         || _s.graphLegendPosition == 'bottom-right'
         || _s.graphLegendPosition == 'top-left'
         || _s.graphLegendPosition == 'bottom-left') {
        options.legend.position = 'inset';
        options.legend.inset = {
          anchor: _s.graphLegendPosition,
          x: 10,
          y: 0
        };
      }
      // Break column if more than x datas.
      if (datas.legend.length <= _s.graphLegendStep) {
        options.legend.inset.step = datas.legend.length;
      }
      else {
        options.legend.inset.step = _s.graphLegendStep;
      }
    }
    // Y axis min and max values.
    if (_s.graphYaxisMax != '') {
      options.axis.y.max = _s.graphYaxisMax;
    }
    if (_s.graphYaxisMin != '') {
      options.axis.y.min = _s.graphYaxisMin;
    }
    // Axis prefixes and suffixes.
    if (_s.graphYaxisPrefix != '' || _s.graphYaxisSuffix != '') {
      options.axis.y.tick.format = function (d) { return _s.graphYaxisPrefix + d + _s.graphYaxisSuffix; }
    }
    // Set Y center value.
    if (_s.graphYaxisCenter != '') {
      options.axis.y.center = _s.graphYaxisCenter;
    }
    // Group options.
    if (_s.graphGroups != '') {
      if (_s.graphGroups instanceof Array) {
        // Associate column id with data names.
        options.data.groups = (_s.graphGroups.map(function(value, index) {
          return value.map(function(d, i) {
            return datas.legend[d - 1];
          });
        }));
      }
    }
    // Y2 axis.
    if (_s.graphY2axisShow == true) {
      // Instanciate y2.
      options.axis.y2 = {
        show: _s.graphY2axisShow,
        padding: {
          top: 10,
          bottom: 10
        },
        tick: {
          outer: false
        }
      };
      if (_s.graphY2axisMax != '') {
        options.axis.y2.max = _s.graphY2axisMax;
      }
      if (_s.graphY2axisMin != '') {
        options.axis.y2.min = _s.graphY2axisMin;
      }
      if (_s.graphY2axisSource != '') {
        // Recover column name from index.
        var obj = {};
        obj[datas.legend[_s.graphY2axisSource - 1]] = 'y2';
        options.data.axes = obj;
      }
    }
    // Lines dotted.
    if (_s.graphLineDashed != undefined && _s.graphLineDashed instanceof Array) {
        var arrayLength = _s.graphLineDashed.length;
        var obj = {};
        for (var i = 0; i < arrayLength; i++) {
          // If this index exist, let set this regions dotted.
          if (datas.legend[_s.graphLineDashed[i]-1] != undefined) {
            obj[datas.legend[_s.graphLineDashed[i]-1]] = [{'style':'dashed'}];
          }
        }
        options.data.regions = obj;
    }

    // Reset settings to avoid child heritage.
    _s.graphDataLabel = false;
    _s.graphInteraction = true;
    _s.graphLegend = true;
    _s.graphTooltip = true;
    _s.graphGroupedTooltip = true;
    _s.graphGridX = false;
    _s.graphGridY = false;
    _s.graphRotated = false;
    _s.graphXaxisShow = true;
    _s.graphXaxisLabel = '';
    _s.graphXaxisCentered = '0';
    _s.graphXaxisRotation = '0';
    _s.graphYaxisShow = true;
    _s.graphYaxisInverted = false;
    _s.graphYaxisLabel = '';
    _s.graphBarRatio = '0.9';
    _s.graphLineDotted = undefined;
    _s.graphDot = true;
    _s.graphDotR = "2.5";
    _s.graphColors = undefined;
    // Extra settings default reset.
    _s.graphRender = 'line';
    _s.graphWidth = _s.graphDefaultWidth;
    _s.graphHeight = _s.graphDefaultHeight;
    _s.graphPadding = undefined;
    _s.graphTypes = '';
    _s.graphLegendPosition = 'bottom';
    _s.graphLegendStep = 30;
    _s.graphYaxisMin = '';
    _s.graphYaxisMax = '';
    _s.graphYaxisCenter = '';
    _s.graphYaxisPrefix = '';
    _s.graphYaxisSuffix = '';
    _s.graphGroups = '';
    _s.graphY2axisShow = false;
    _s.graphY2axisMin = '';
    _s.graphY2axisMax = '';
    _s.graphY2axisSource = '';

    // Use C3 to build our graph.
    console.log(options);
    //console.log(_s);
    //console.log(datas);
    var chart = c3.generate(options);
  }

})(jQuery, Drupal);
